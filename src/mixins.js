export const apiCallList = {
    data() {
        return {
            data: null,
        }
    },
    created() {
        this.fetch();
    },
    methods: {
        fetch() {
            this.$axios.get(`/${this.endpoint}`)
            .then((response) => {
                this.data = response.data;
                this.loadItems();
            }).catch(()=> {
                this.data = [];
                this.loadItems();
            })
        },  
    }
}

