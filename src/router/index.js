import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('../views/products.vue')
  },
  {
    path: '/market',
    component: () => import('../views/markets.vue')
  },
  {
    path: '/products',
    component: () => import('../views/productPrice.vue')
  },
  {
    path : "/formulas",
    component: () => import("../views/formulas/formulasList.vue")
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
